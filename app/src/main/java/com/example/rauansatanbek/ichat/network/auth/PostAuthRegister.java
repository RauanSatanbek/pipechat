package com.example.rauansatanbek.ichat.network.auth;

import android.util.Log;

import com.example.rauansatanbek.ichat.Consts;
import com.example.rauansatanbek.ichat.models.network.auth.AuthModel;
import com.example.rauansatanbek.ichat.models.user.UserSignUp;
import com.example.rauansatanbek.ichat.network.ApiClient;
import com.example.rauansatanbek.ichat.network.ApiService;

import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Rauan Satanbek on 28.04.2017.
 */

public class PostAuthRegister {
    //body
    UserSignUp userSignUp;
    MultipartBody.Part photo;
    HashMap<String, RequestBody> body;

    //interface
    PostAuthRegisterInterface mInterface;
    /** Constructor */
    public PostAuthRegister(PostAuthRegisterInterface mInterface, UserSignUp userSignUp, MultipartBody.Part photo) {
        this.mInterface = mInterface;
        this.photo = photo;
        this.userSignUp = userSignUp;

        this.body = new HashMap<>();
        body.put("first_name", createRequestBody(userSignUp.getFirstName()));
        body.put("last_name", createRequestBody(userSignUp.getLastName()));
        body.put("email", createRequestBody(userSignUp.getEmail()));
        body.put("password", createRequestBody(userSignUp.getPassword()));
        body.put("date_of_birth", createRequestBody("2017-01-10"));
    }

    /** Auth register interface */
    public interface PostAuthRegisterInterface{
        /** this method will be called
         * when we got response from server in the Activity or Fragment*/
        void postAuthRegisterInterface(AuthModel authModel);
    }

    /** Auth register */
    public void postAuthRegister() {
        ApiService service = ApiClient.initApiService();
        Call<AuthModel> call = service.postAuthRegister(body, photo);

        if(userSignUp.getTypeOfSignUp() == Consts.SIGNUP_WITHOUT_PHOTO) {
            call = service.postAuthRegisterWithOutPhoto(userSignUp);
        }
        call.enqueue(new Callback<AuthModel>() {
            @Override
            public void onResponse(Call<AuthModel> call, Response<AuthModel> response) {
                try {
                    Log.d("MyLogs", "postAuthRegister: response.errorBody: " + response.errorBody().string());
                } catch (Exception e) {}
                Log.d("MyLogs", "postAuthRegister: response: " + response.isSuccessful() + "\n" +
                response.message() + " " + response.errorBody());

                //if response success
                if(response.isSuccessful()) mInterface.postAuthRegisterInterface(response.body());
                //Or
                else mInterface.postAuthRegisterInterface(null);
            }

            @Override
            public void onFailure(Call<AuthModel> call, Throwable t) {
                Log.d("MyLogs", "Throwable: " + t.toString());
                //if response error
                mInterface.postAuthRegisterInterface(null);
            }
        });
    }

    RequestBody createRequestBody(String value) {
        return RequestBody.create(MediaType.parse("text"),value.getBytes());
    }
}
