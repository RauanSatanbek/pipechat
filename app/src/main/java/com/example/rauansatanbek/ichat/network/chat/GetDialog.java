package com.example.rauansatanbek.ichat.network.chat;

import android.util.Log;

import com.example.rauansatanbek.ichat.models.chat.DialogList;
import com.example.rauansatanbek.ichat.network.ApiClient;
import com.example.rauansatanbek.ichat.network.ApiService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Rauan Satanbek on 14.05.2017.
 */

public class GetDialog {
    GetDialogInterface mInterface; //callBack
    String token; //token
    int dialog_id; //dialog id

    /** Constructor
     */
    public GetDialog(GetDialogInterface mInterface, String token, int dialog_id) {
        this.mInterface = mInterface;
        this.token = token;
        this.dialog_id = dialog_id;
    }

    public interface GetDialogInterface{
        /** this method will be called
         * when we got response from server in the Activity or Fragment*/
        void getDialog(DialogList dialogList);
    }

    public void getDialog() {
        ApiService service = ApiClient.initApiService();
        Call<DialogList> call = service.getDialog(token, dialog_id);
        call.enqueue(new Callback<DialogList>() {
            @Override
            public void onResponse(Call<DialogList> call, Response<DialogList> response) {
                try {
                    Log.d("MyLogs", "getDialog: response.Body: " + response.body().getMessages().get(0).getMessage());
                    Log.d("MyLogs", "getDialog: response.errorBody: " + response.errorBody().string() );
                } catch (Exception e) {}
                Log.d("MyLogs", "getDialog: response: " + response.isSuccessful() + "\n" +
                        response.message() + " " + response.code());


                //if response success
                if(response.isSuccessful()) mInterface.getDialog(response.body());
                //Or
                else mInterface.getDialog(null);
            }

            @Override
            public void onFailure(Call<DialogList> call, Throwable t) {
                //if response error
                Log.d("MyLogs", "Throwable: " + t.toString());
                mInterface.getDialog(null);
            }
        });
    }
}
