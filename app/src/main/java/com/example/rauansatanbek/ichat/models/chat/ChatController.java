package com.example.rauansatanbek.ichat.models.chat;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.databinding.BaseObservable;
import android.speech.RecognizerIntent;
import android.view.View;

import com.example.rauansatanbek.ichat.Consts;
import com.example.rauansatanbek.ichat.R;
import com.example.rauansatanbek.ichat.activities.chat.ChatActivity;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.engine.impl.GlideEngine;

import java.util.Locale;

import static com.example.rauansatanbek.ichat.Consts.REQ_CODE_SPEECH_INPUT;

public class ChatController extends BaseObservable {

    /** attach file */
    public void attachFile(View view, Chat chat){
        //finding activity
        Context context = view.getContext();
        ChatActivity chatActivity = (ChatActivity) context;

        //change to !attachFile
        chat.setAttachFile(!chat.attachFile);
    }

    /** run matisse */
    public void runMatisse(View view){
        //finding activity
        Context context = view.getContext();
        ChatActivity chatActivity = (ChatActivity) context;

        //run matisse
        Matisse.from(chatActivity)
                .choose(MimeType.ofAll())
                .theme(R.style.Matisse_Zhihu)
                .countable(false)
                .maxSelectable(10)
                .imageEngine(new GlideEngine())
                .forResult(Consts.ATTACHED_FILE);
    }

    // Showing google speech input dialog
    public void askSpeechInput(View view){
        //finding activity
        Context context = view.getContext();
        ChatActivity chatActivity = (ChatActivity) context;
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                "Hi speak something");
        try {
            chatActivity.startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {

        }
    }
}
