package com.example.rauansatanbek.ichat.views.adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.rauansatanbek.ichat.R;
import com.example.rauansatanbek.ichat.databinding.ContactsItemListBinding;
import com.example.rauansatanbek.ichat.models.chat.Message;
import com.example.rauansatanbek.ichat.models.user.UserList;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;

public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.ContactsViewHolder>{
    List<UserList> mUserList; // list of users
    HashMap<Integer, ContactsAdapter.ContactsViewHolder> mHolders; // list of users
    Context context; // context

    ContactsOnClick contactsOnClick; // contacts on click

    public interface ContactsOnClick {
        void contactsOnClick(int id);
    }

    public ContactsAdapter(Context context,  List<UserList> mUserList) {
        this.mUserList = mUserList;
        this.contactsOnClick = (ContactsOnClick) context;
        this.context = context;
        this.mHolders = new HashMap<>();
    }

   /** Add item to adapter
     */
    public void addItem(UserList userList) {
        this.mUserList.add(0, userList);
        notifyItemInserted(0);
    }

   /** Remove item from adapter
     */
    public void removeItem(int userId) {
        try {
            if (mHolders.get(userId) != null) {
                this.mUserList.remove(mHolders.get(userId).getAdapterPosition());
                notifyItemRemoved(mHolders.get(userId).getAdapterPosition());
            }
        } catch (Exception e) {
            Log.d("ContactsAdapter", "removeItem: " + e.toString());
        }
    }

   /** Update Adapter
     */
    public void updateList(List<UserList> mUserList) {
        this.mUserList = mUserList;
        notifyItemRangeChanged(0, mUserList.size());
    }

  /** contactToUP
    * when click to contact or
    * when we got message from socket
    */
   private void contactToUP(int position, boolean onClick) {
       UserList list = mUserList.get(position);
       if (position != 0) {
           mUserList.remove(position);
           notifyItemRemoved(position);
           mUserList.add(0, list);
           notifyItemInserted(0);
//            notifyItemRangeChanged(0, mUserList.size());
       } else {
           notifyItemChanged(0);
       }
       
       if (onClick) {
           contactsOnClick.contactsOnClick(list.getId());
           mUserList.get(0).setMessage(null);
       }
    }

   /** getContactItemBinding
     * return the binding to manipulate with contact
     */
    public void contactNewMessage(Message message) {
        try {
            int position = mHolders.get(message.getSender()).getAdapterPosition();
            mUserList.get(position).setMessage(message);
            mUserList.get(position).setMessageCount(mUserList.get(position).getMessageCount() + 1);
            contactToUP(position, false);
        } catch (Exception e) {
            Log.d("ContactsAdapter", "getMessageCount: " + e.toString());
        }

    }

    @Override
    public ContactsAdapter.ContactsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflater
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        //view
        View v = inflater.inflate(R.layout.contacts_item_list, parent, false);

        return new ContactsViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ContactsAdapter.ContactsViewHolder holder, int position) {
        UserList userList = mUserList.get(position);
        Message message = userList.getMessage();
        holder.contactsBinding.setUser(userList);

        if (userList.getPhoto() != null) {
            Log.d("MyLogs", "Photo: " + (userList.getPhoto()));
            Picasso.with(context)
                    .load(userList.getPhoto())
                    .placeholder(R.drawable.user)
                    .error(R.drawable.user)
                    .into(holder.contactsBinding.userAvatar);
        }

        if (message != null) {
            String countStr = userList.getMessageCount() + "";
            holder.contactsBinding.userCountOfMessage.setText(countStr);
            holder.contactsBinding.userCountOfMessage.setVisibility(View.VISIBLE);
            int messageTime[][] = ChatMsgDialogAdapter.getDateFromUTC(message.getCreatedAt());
            holder.contactsBinding.userLastMessage.setText(message.getMessage());
            holder.contactsBinding.userLastMessageTime.setText(messageTime[1][0] + ":" + messageTime[1][1]);
        } else {
            holder.contactsBinding.userCountOfMessage.setVisibility(View.GONE);
            userList.setMessageCount(0);
        }

        mHolders.put(userList.getId(), holder); // holder list
    }

    @Override
    public int getItemCount() {
        return mUserList.size();
    }

    class ContactsViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener{
        ContactsItemListBinding contactsBinding;
        ContactsViewHolder(View itemView) {
            super(itemView);
            //bind layout
            contactsBinding = DataBindingUtil.bind(itemView);

            contactsBinding.containerContacts.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            contactToUP(getAdapterPosition(), true);
        }
    }
}
