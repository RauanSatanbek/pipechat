package com.example.rauansatanbek.ichat.service;


import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.example.rauansatanbek.ichat.Consts;
import com.example.rauansatanbek.ichat.R;
import com.example.rauansatanbek.ichat.models.chat.MessageFromSocket;
import com.example.rauansatanbek.ichat.models.user.UserList;
import com.example.rauansatanbek.ichat.network.real_time.RealTimeCallBacks;
import com.example.rauansatanbek.ichat.network.real_time.RealTimeClient;
import com.example.rauansatanbek.ichat.network.real_time.RealTimeListener;
import com.example.rauansatanbek.ichat.singleton.SingletonSharedPref;

import okhttp3.OkHttpClient;

import static com.example.rauansatanbek.ichat.singleton.SingletonSharedPref.Key.AUTH_TOKEN;

public class MessageService extends Service implements RealTimeCallBacks{
    //shared pref
    SingletonSharedPref sharedPref;
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    String messageForBot[] = {};
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("MyLogs", "onStartCommand");
//        MyRun myRun = new MyRun(intent.getStringExtra("messageFromUser"));
//        Thread thread = new Thread(myRun);
//        thread.start();

        //singleton shared pref
        if(sharedPref == null)
            sharedPref = SingletonSharedPref.getInstance(this);

        OkHttpClient client = new OkHttpClient();
        RealTimeClient realTimeClient = new RealTimeClient(sharedPref.getString(AUTH_TOKEN));
        RealTimeListener webSocketListener = new RealTimeListener(this);

        client.newWebSocket(realTimeClient.getRequest(), webSocketListener);
        client.dispatcher().executorService().shutdown();

        //Test sendBroadcast
//        Intent i = new Intent("pipe.chat.message.service");
//        Message message = new Message("Hello from service");
//        i.putExtra("message", message);
//        sendBroadcast(i);
        return START_STICKY;
    }

//    class MyRun implements Runnable , RealTimeCallBacks {
//        String messageFromUser;
//        MyRun(String messageFromUser) {
//            this.messageFromUser = messageFromUser;
//        }
//        @Override
//        public void run() {
//            try {
//
//                Random random = new Random();
//                int indexAndTime = random.nextInt(messageForBot.length);
//                Log.d("MyLogs", "indexAndTime = " + indexAndTime);
//                Thread.sleep(indexAndTime * 1000);
//                String messageFromBot = messageForBot[indexAndTime];
//                if(messageFromUser.equals("Hi") || messageFromUser.equals("hi") || messageFromUser.equals("HI")) {
//                    messageFromBot = "Hi bro";
//                    bye = false;
//                } else if(messageFromUser.equals("Bye")) {
//                    messageFromBot = "Bye bro";
//                    bye = true;
//                }
//
//                Intent intent = new Intent("chat.with.bot.random");
//                intent.putExtra("messageFromBot", messageFromBot);
//                intent.putExtra("who", BOT);
//                intent.putExtra("name", "BOT");
//
//                NotificationCompat.Builder mBuilder =
//                        (NotificationCompat.Builder) new NotificationCompat.Builder(MessageService.this)
//                                .setContentTitle("Message from Bot")
//                                .setContentInfo(messageFromBot)
//                                .setContentText("Bot");
//
//                NotificationManager mNotificationManager =
//                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//                if(messageFromUser.equals("Bye")) {
//                    mNotificationManager.notify(1, mBuilder.build());
//                    sendBroadcast(intent);
//                }
//                if(!bye) {
//                    mNotificationManager.notify(1, mBuilder.build());
//                    sendBroadcast(intent);
//                }
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//
//        }

//    }

    @Override
    public void chatDialogCreated() {

    }

    @Override
    public void chatMessageCreated(MessageFromSocket message) {
        try {
            Log.d("MyLogs", "Service: chatMessageCreated: " + message.getMessage());
            NotificationCompat.Builder mBuilder =
                    (NotificationCompat.Builder) new NotificationCompat.Builder(MessageService.this)
                            .setContentTitle(message.getUser().getFirstName() + " " + message.getUser().getLastName())
                            .setSmallIcon(R.drawable.ic_arrow_down)
                            .setContentText(message.getMessage().getMessage());

            NotificationManager mNotificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager.notify(1, mBuilder.build());

            Intent intent = new Intent("pipe.chat.message.service");
            intent.putExtra("message", message);
            intent.putExtra("status", Consts.SOCKET_CREATE_MESSAGE);
            sendBroadcast(intent);
        } catch(Exception e) {
            Log.d("MessageService", "ChatMessageCreated: " + e.toString());
        }
    }

    @Override
    public void contactsUserOnline(UserList user) {
        try {
            Log.d("MessageService", "userOnline: " + user.getFirstName());
            Intent intent = new Intent("pipe.chat.message.service");
            intent.putExtra("user", user);
            intent.putExtra("status", Consts.SOCKET_USER_ONLINE);
            sendBroadcast(intent);
        } catch(Exception e) {
            Log.d("MessageService", "ChatMessageCreated: " + e.toString());
        }
    }

    @Override
    public void contactsUserOffline(UserList user) {
        try {
            Log.d("MessageService", "userOffline: " + user.getFirstName());
            Intent intent = new Intent("pipe.chat.message.service");
            intent.putExtra("user", user);
            intent.putExtra("status", Consts.SOCKET_USER_OFFLINE);
            sendBroadcast(intent);
        } catch(Exception e) {
            Log.d("MessageService", "contactsUserOffline: " + e.toString());
        }
    }
}
