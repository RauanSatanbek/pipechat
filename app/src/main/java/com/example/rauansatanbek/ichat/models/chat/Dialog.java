
package com.example.rauansatanbek.ichat.models.chat;

import com.example.rauansatanbek.ichat.models.user.UserList;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Dialog {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("participants")
    @Expose
    private List<UserList> participants = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<UserList> getParticipants() {
        return participants;
    }

    public void setParticipants(List<UserList> participants) {
        this.participants = participants;
    }

}
