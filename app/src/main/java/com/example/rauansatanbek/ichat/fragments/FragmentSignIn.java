package com.example.rauansatanbek.ichat.fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.rauansatanbek.ichat.R;
import com.example.rauansatanbek.ichat.databinding.FragmentSigninBinding;
import com.example.rauansatanbek.ichat.models.user.UserSignIn;
import com.example.rauansatanbek.ichat.models.user.UserSignInController;

public class FragmentSignIn extends Fragment{
    FragmentSigninBinding signInBinding;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        signInBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_signin, container, false);

        //user sign in data
        UserSignIn userSignIn = new UserSignIn();
        UserSignInController userSignInController = new UserSignInController();

        //set data
        signInBinding.setSignIn(userSignIn);
        signInBinding.setController(userSignInController);
        return signInBinding.getRoot();
    }

    public void stopProcess() {
        signInBinding.signIn.setProgress(0);
    }
}
