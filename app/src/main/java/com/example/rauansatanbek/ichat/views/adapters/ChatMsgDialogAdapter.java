package com.example.rauansatanbek.ichat.views.adapters;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.rauansatanbek.ichat.Consts;
import com.example.rauansatanbek.ichat.R;
import com.example.rauansatanbek.ichat.models.chat.Message;
import com.example.rauansatanbek.ichat.singleton.SingletonSharedPref;
import com.example.rauansatanbek.ichat.views.holders.ChatMsgDialogViewHolder;
import com.squareup.picasso.Picasso;

import java.util.List;

import static com.example.rauansatanbek.ichat.network.ApiClient.BASE_URL_FOR_LOAD_PHOTO;
import static com.example.rauansatanbek.ichat.singleton.SingletonSharedPref.Key.USER_ID;

public class ChatMsgDialogAdapter extends RecyclerView.Adapter<ChatMsgDialogViewHolder>  {
    SingletonSharedPref sharedPref; //shared pref
    Context context; // context
    LayoutInflater inflater; //inflater

    LinearLayout messageRight, messageLeft;  //messages
    List<Message> messages;

    int[] mDate = {0, 0, 0};
    //data
    public ChatMsgDialogAdapter(Context context, List<Message> messages) {
        this.context = context;
        this.messages = messages;
    }

    public void addMessageItem(Message message) {
        messages.add(message);
        this.notifyItemInserted(messages.size() - 1);
    }

    public int getMessagesSize() {
        return messages.size();
    }
    @Override
    public ChatMsgDialogViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //shared pref
        if (sharedPref == null)
            sharedPref = SingletonSharedPref.getInstance();

        //inflater
        inflater = LayoutInflater.from(parent.getContext());

        //inflate view
        View v = inflater.inflate(R.layout.chat_message_item, parent, false);

        //return ViewHolder
        return new ChatMsgDialogViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ChatMsgDialogViewHolder holder, int position) {
        //message container
        LinearLayout container = holder.chatBinding.itemMessageContainer;
        Message message = messages.get(position);
        //convert UTC to normal
        int[][] createdAt = getDateFromUTC(message.getCreatedAt());
        int[] messageDate = createdAt[0];
        int[] messageTime = createdAt[1];

        //clear container
        container.removeAllViews();

        //add view to container message right or left
        LinearLayout msg = getMessageView(message.getSender(), container);
        LinearLayout msg_delivery = getMessageDeliveryView(message.getSender(), container, position, messageTime);

        TextView msg_text = (TextView) msg.findViewById(R.id.chat_message_text);
        ImageView msg_img = (ImageView) msg.findViewById(R.id.chat_message_img);
        CardView msg_card = (CardView) msg.findViewById(R.id.card_view);
        //add dateView to container
        //TODO test
        if (mDate[1] != messageDate[1] || mDate[2] != messageDate[2]) {
            Log.d("MyLogs", "messageDate: " + mDate[1] + " " + messageDate[1] + " : " + mDate[2] + " " + messageDate[2] + " " + position + " " +
                    (mDate[1] != messageDate[1]) + " " + (mDate[2] != messageDate[2]));
            mDate = messageDate;
            TextView tvDate = getMessageDateView(container);
            tvDate.setText(mDate[2] + " " + Consts.MONTHS[mDate[1] - 1]);
            container.addView(tvDate);
//            msg_img.setVisibility(View.VISIBLE);
        }
        //add dateView to container
//        if (position % 5 == 0)  {
//            TextView date = getMessageDateView(container);
//            date.setText(++position + " JULE");
//            container.addView(date);
//            msg_img.setVisibility(View.VISIBLE);
//        }

        //setPhoto
        Log.d("MyLogs", "message.getType(): " + (message.getType() == Consts.MESSAGE_PHOTO ? "Photo" : "Text"));
        if (message.getType() == Consts.MESSAGE_PHOTO) {
            msg_text.setVisibility(View.GONE);
            setPhoto(msg_img, msg_card, message);

        } else if(message.getType() == Consts.MESSAGE_TEXT) {
            msg_text.setText(getMessageText(message));
        }

        container.addView(msg);
        container.addView(msg_delivery);
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    /**
     * checking: if user is you return message right or message left
     */
    private LinearLayout getMessageView(int user_id, LinearLayout parent) {
        //messages
        messageRight = (LinearLayout) inflater.inflate(R.layout.chat_message_right, parent, false);
        messageLeft = (LinearLayout) inflater.inflate(R.layout.chat_message_left, parent, false);

//        if (user_id == sharedPref.getInt(USER_ID, 1)) return messageRight;
        if (user_id == sharedPref.getInt(USER_ID)) return messageRight;
        return messageLeft;
    }

    /**
     * get message date view
     */
    private TextView getMessageDateView(LinearLayout parent) {
        //date view
        return (TextView) inflater.inflate(R.layout.chat_message_date, parent, false);
    }

    /**
     * get message delivery view right or left
     */
    private LinearLayout getMessageDeliveryView(int user_id, LinearLayout parent, int position, int[] time) {
        LinearLayout left = (LinearLayout) inflater.inflate(R.layout.chat_message_delivery_left, parent, false);
        LinearLayout right = (LinearLayout) inflater.inflate(R.layout.chat_message_delivery_right, parent, false);

        ImageView done = (ImageView) right.findViewById(R.id.ic_done);
        ImageView done_all = (ImageView) right.findViewById(R.id.ic_done_all);
        TextView message_delivery = (TextView) right.findViewById(R.id.message_delivery);
        message_delivery.setText("Delivered " + time[0] + ":" + time[1]);
        //TODO is seen
        if (position % 3 == 0) {
            done.setVisibility(View.GONE);
            done_all.setVisibility(View.VISIBLE);
            message_delivery.setText("Seen " + time[0] + ":" + time[1]);
        }
        //delivery view
        if (user_id == sharedPref.getInt(USER_ID)) return right;
//        if (user_id == sharedPref.getInt(USER_ID, 1)) return right;
        return left;
    }


    /** Convert UTC to date and time
     * "2017-05-16T05:04:47.877490Z" -> "2017-05-16" & "05:04";
     */
    public static int[][] getDateFromUTC(String UTC) {
        String[] m = UTC.split("T");
        String[] d = m[0].split("-");
        String[] t = m[1].split(":");

        return new int[][]{
                {toInt(d[0]), toInt(d[1]), toInt(d[2])},
                {toInt(t[0]), toInt(t[1])}
        };
    }

    public static int toInt(String number) { return Integer.parseInt(number); }
    /**
     * get text from Message module
     */
    private String getMessageText(Message message) {
        return message.getMessage();
    }

    private void setPhoto(ImageView imageView, CardView msg_card, Message message) {
        if(message.getTypeOfPhoto() == Consts.PHOTO_FROM_GALLERY) {
//            Log.d("MyLogs", "setPhoto: " + message.getImage().getAbsolutePath());
            imageView.setImageBitmap(BitmapFactory.decodeFile(message.getImage().getAbsolutePath()));
//            imageView.setImageURI(message.getPhotoUri().get(0));
            msg_card.setVisibility(View.VISIBLE);
        } else if (message.getTypeOfPhoto() == Consts.PHOTO_FROM_SERVER) {
//            Log.d("MyLogs", "setPhoto: PHOTO_FROM_SERVER: " + message.getPhoto());
            String base_url = BASE_URL_FOR_LOAD_PHOTO;

            if(message.getComeFrom() == Consts.MESSAGE_COME_FROM_SOCKET) {
                base_url = "";
            }

            Picasso.with(context)
                    .load(base_url + message.getPhoto())
                    .into(imageView);

            msg_card.setVisibility(View.VISIBLE);
        } else {
//            Log.d("MyLogs", "setPhoto: no photo: " + message.getTypeOfPhoto());
            msg_card.setVisibility(View.GONE);
        }
    }
}
