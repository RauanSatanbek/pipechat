package com.example.rauansatanbek.ichat.network;

import com.example.rauansatanbek.ichat.models.chat.DialogList;
import com.example.rauansatanbek.ichat.models.chat.Message;
import com.example.rauansatanbek.ichat.models.chat.Participants;
import com.example.rauansatanbek.ichat.models.network.auth.AuthModel;
import com.example.rauansatanbek.ichat.models.user.UserList;
import com.example.rauansatanbek.ichat.models.user.UserSignUp;

import java.util.HashMap;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;

public interface ApiService {
    /** Auth register */
    @Multipart
    @POST("api/auth/register/")
    Call<AuthModel> postAuthRegister(
            @PartMap HashMap<String, RequestBody> body,
            @Part MultipartBody.Part photo
    );

    @POST("api/auth/register/")
    Call<AuthModel> postAuthRegisterWithOutPhoto(
            @Body UserSignUp userSignUp
    );

    /** Auth login */
    @POST("api/auth/login/")
    Call<AuthModel> postAuthLogin(
            @Header("Authorization") String token
    );

    /** Get list of users*/
    @GET("api/auth/")
    Call<List<UserList>> getUserList(
            @Header("Authorization") String token
    );

    /** Post create dialog */
    @POST("api/dialog/")
    Call<DialogList> postCreateDialog(
            @Header("Authorization") String token,
            @Body Participants participants
    );

    @GET("api/dialog/{id}/")
    Call<DialogList> getDialog(
            @Header("Authorization") String token,
            @Path("id") int id
    );

    /** Post create message */
    @POST("api/message/")
    Call<Message> postCreateMessage(
            @Header("Authorization") String token,
            @Body HashMap<String, String> body
    );

    @Multipart
    @POST("api/message/")
    Call<Message> postCreateMessagePhoto(
            @Header("Authorization") String token,
            @PartMap HashMap<String, RequestBody> body,
            @Part MultipartBody.Part photo
    );
}
