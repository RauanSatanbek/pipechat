package com.example.rauansatanbek.ichat.fragments;

import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.rauansatanbek.ichat.R;
import com.example.rauansatanbek.ichat.databinding.FragmentSignupBinding;
import com.example.rauansatanbek.ichat.models.user.UserSignUp;
import com.example.rauansatanbek.ichat.models.user.UserSignUpController;

public class FragmentSignUp extends Fragment implements View.OnClickListener{
    public FragmentSignupBinding signupBinding;

    //data
    UserSignUp signUp;
    UserSignUpController controller;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        signupBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_signup, container, false);

        //data
        signUp = new UserSignUp();
        controller = new UserSignUpController();

        //set data
        signupBinding.setSignUp(signUp);
        signupBinding.setController(controller);

        return signupBinding.getRoot();
    }

    @Override
    public void onClick(View v) {

    }

    public void setAvatar(Uri uri) {
        signupBinding.userAvatar.setImageURI(uri);
    }

    public void stopProcess() {
        signupBinding.signUp.setProgress(0);
    }
}
