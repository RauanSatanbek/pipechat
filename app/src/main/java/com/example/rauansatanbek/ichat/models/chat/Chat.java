package com.example.rauansatanbek.ichat.models.chat;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.util.Log;

import com.example.rauansatanbek.ichat.BR;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Chat extends BaseObservable {
    @SerializedName("text")
    @Expose
    String text = "";

    boolean attachFile = false;
    boolean isGotMessage = false;
    @Bindable
    public String getText() {return text;}

    public void setText(String text) {
        this.text = text;
        Log.d("MyLogs", "text: " + text);
        notifyPropertyChanged(BR.text);
    }

    @Bindable
    public boolean getAttachFile() {return attachFile;}

    public void setAttachFile(boolean attachFile) {
        this.attachFile = attachFile;
        notifyPropertyChanged(BR.attachFile);
    }

    @Bindable
    public boolean getIsGotMessage() {return isGotMessage;}

    public void setIsGotMessage(boolean isGotMessage) {
        this.isGotMessage = isGotMessage;
        notifyPropertyChanged(BR.isGotMessage);
    }
}
