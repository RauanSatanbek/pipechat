package com.example.rauansatanbek.ichat.network.real_time;

//"event": "Discount.UpdateRespond"
//        "data": {
//        "discount": 10,
//        "respond_count": 1
//        }
//
//        "event": "Discount.submit"
//        Discount
//
//        "event": "Discount.UpdateSubmit"
//        "data": {
//        "discount": 10,
//        "is_active": true,
//        "submitted_count": 1
//        }
//        "event": "QR.Check"
//        "data": {
//        "discount_id": 10,
//        "accept": true
//        }
//        "event": "Discount.NotActive"
//        "data": {"discount": 14}

import com.example.rauansatanbek.ichat.models.chat.MessageFromSocket;
import com.example.rauansatanbek.ichat.models.user.UserList;

//200, 'Dialog.Created', {"dialog": serializer.data}
//200, 'Message.Created', {"message": serializer.data}
public interface RealTimeCallBacks {
    void chatDialogCreated ();
    void chatMessageCreated (MessageFromSocket message);
    void contactsUserOnline (UserList user);
    void contactsUserOffline (UserList user);
}
