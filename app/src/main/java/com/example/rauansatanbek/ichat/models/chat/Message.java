package com.example.rauansatanbek.ichat.models.chat;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.net.Uri;

import com.example.rauansatanbek.ichat.BR;
import com.example.rauansatanbek.ichat.Consts;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.File;
import java.io.Serializable;
import java.util.List;

public class Message extends BaseObservable implements Serializable {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("type")
    @Expose
    private Integer type;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("sender")
    @Expose
    private Integer sender;
    @SerializedName("dialog")
    @Expose
    private Integer dialog;

    @SerializedName("photo")
    @Expose
    String photo = "";

    List<Uri> photoUri;
    File image = null;
    int typeOfPhoto = Consts.PHOTO_FROM_SERVER;
    int comeFrom = 0;

    public Message(String text) {
        setMessage(text);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Integer getSender() {
        return sender;
    }

    public void setSender(Integer sender) {
        this.sender = sender;
    }

    public Integer getDialog() {
        return dialog;
    }

    public void setDialog(Integer dialog) {
        this.dialog = dialog;
    }


    @Bindable
    public String getPhoto() {return photo;}

    public void setPhoto(String photo) {
        this.photo = photo;
        notifyPropertyChanged(BR.photo);
    }

    public List<Uri> getPhotoUri() {return photoUri;}

    public void setPhotoUri(List<Uri> photoUri) {
        this.photoUri = photoUri;
    }

    public int getTypeOfPhoto() {return typeOfPhoto;}

    public void setTypeOfPhoto(int typeOfPhoto) {
        this.typeOfPhoto = typeOfPhoto;
    }

    public File getImage() {return image;}

    public void setImage(File image) {
        this.image = image;
    }


    public int getComeFrom() {return comeFrom;}

    public void setComeFrom(int comeFrom) {
        this.comeFrom = comeFrom;
    }
}

