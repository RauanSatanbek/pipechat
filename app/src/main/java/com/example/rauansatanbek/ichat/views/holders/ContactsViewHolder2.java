package com.example.rauansatanbek.ichat.views.holders;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.rauansatanbek.ichat.databinding.ContactsItemListBinding;

/**
 * Created by Rauan Satanbek on 02.05.2017.
 */

public class ContactsViewHolder2 extends RecyclerView.ViewHolder  implements View.OnClickListener{
    public ContactsItemListBinding contactsBinding;
    public ContactsViewHolder2(View itemView) {
        super(itemView);

        //bind layout
        contactsBinding = DataBindingUtil.bind(itemView);
    }

    @Override
    public void onClick(View v) {
        int position = getAdapterPosition();
//        UserList list = mUserList.get(get)
//        Log.d("MyLogs", "OnCLick: " + )
    }
}
