package com.example.rauansatanbek.ichat.network.real_time;

import android.util.Log;

import okhttp3.Request;

public class RealTimeClient {
    private static String REALTIME_URL = "ws://powerful-mesa-27755.herokuapp.com/api/stream/?token=";
    private static Request request ;
    public RealTimeClient(String USER_TOKEN) {
        REALTIME_URL += (USER_TOKEN.split(" ")[1]);

        Log.d("MyLogs", REALTIME_URL);
    }

    public Request getRequest() {
        if(request == null){
            request = new Request.Builder()
                    .url(REALTIME_URL)
                    .build();
        }

        return request;
    }


}