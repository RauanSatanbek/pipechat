package com.example.rauansatanbek.ichat.models.chat;

import android.databinding.BaseObservable;

import com.example.rauansatanbek.ichat.models.user.UserList;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MessageFromSocket extends BaseObservable implements Serializable {
    @SerializedName("user")
    @Expose
    private UserList user;

    @SerializedName("message")
    @Expose
    private Message message;

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }
    public UserList getUser() {
        return user;
    }

    public void setUser(UserList user) {
        this.user = user;
    }
}
