package com.example.rauansatanbek.ichat.models.user;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.example.rauansatanbek.ichat.BR;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserSignIn extends BaseObservable {
    @SerializedName("email")
    @Expose
    public String email;

    @SerializedName("password")
    @Expose
    public String password;

    @Bindable
    public String getEmail() {
        return this.email;
    }


    @Bindable
    public String getPassword() {
        return this.password;
    }

    public void setEmail(String email) {
        this.email = email;
        notifyPropertyChanged(BR.email);
    }

    public void setPassword(String password) {
        this.password = password;
        notifyPropertyChanged(BR.password);
    }

    /** checking edit text for empty*/
    public boolean check() {
        return email != null && password != null;
    }
}
