package com.example.rauansatanbek.ichat.views.alert_dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;

import com.example.rauansatanbek.ichat.R;
import com.example.rauansatanbek.ichat.models.user.UserSignUp;

public class ShowDataPicker extends DialogFragment implements AlertDialog.OnClickListener {
    DatePicker datePicker;
    UserSignUp userSignUp;
    public ShowDataPicker(UserSignUp userSignUp) {
        this.userSignUp = userSignUp;
    }
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.data_picker, null);
        datePicker = (DatePicker) view.findViewById(R.id.datePicker);

        builder.setView(view);
        builder.setPositiveButton("Ok", this);
        builder.setNegativeButton("Cancel", this);
        return builder.create();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        Log.d("MyLogs", "onClick which = " + which);
        switch (which) {
            case -1:
                int day = datePicker.getDayOfMonth();
                int month = datePicker.getMonth() + 1;
                int year = datePicker.getYear();
//                userSignUp.setDateOfBirth(year + "-" + (month < 10 ? "0" + month : month) + "-" + day);
                break;
            case -2:
                break;
        }
    }
}