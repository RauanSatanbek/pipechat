
package com.example.rauansatanbek.ichat.models.chat;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Participants {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("participants")
    @Expose
    private List<Integer> participants = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Integer> getParticipants() {
        return participants;
    }

    public void setParticipants(List<Integer> participants) {
        this.participants = participants;
    }

}
