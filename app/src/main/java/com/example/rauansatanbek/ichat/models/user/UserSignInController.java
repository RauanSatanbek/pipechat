package com.example.rauansatanbek.ichat.models.user;

import android.content.Context;
import android.databinding.BaseObservable;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.dd.processbutton.iml.ActionProcessButton;
import com.example.rauansatanbek.ichat.Consts;
import com.example.rauansatanbek.ichat.activities.auth.LoginActivity;

public class UserSignInController extends BaseObservable {

    /** sign in */
    public void signIn(View view, UserSignIn user){
        Log.d("MyLogs", "OnClick: ");
        Toast.makeText(view.getContext(), "Hi " + user.email, Toast.LENGTH_LONG).show();
        Context context = view.getContext();
        LoginActivity loginActivity = (LoginActivity) context;
        if(user.check()) {
            ((ActionProcessButton) view).setProgress(50);
            //login
            loginActivity.authLogin(user);
        } else {
            loginActivity.showSnackBar("Fill in all the fields");
        }
    }

    /** don't have an account */
    public void newAccount(View view){
        //finding activity
        Context context = view.getContext();
        LoginActivity loginActivity = (LoginActivity) context;

        //begin transaction
        loginActivity.runTransaction(Consts.FRAGMENT_REPLACE, Consts.FRAGMENT_SIGN_UP);
    }
}
