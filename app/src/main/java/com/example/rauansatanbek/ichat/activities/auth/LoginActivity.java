package com.example.rauansatanbek.ichat.activities.auth;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.rauansatanbek.ichat.Consts;
import com.example.rauansatanbek.ichat.R;
import com.example.rauansatanbek.ichat.activities.chat.ChatActivity;
import com.example.rauansatanbek.ichat.databinding.ActivityLoginBinding;
import com.example.rauansatanbek.ichat.fragments.FragmentSignIn;
import com.example.rauansatanbek.ichat.fragments.FragmentSignUp;
import com.example.rauansatanbek.ichat.models.network.auth.AuthModel;
import com.example.rauansatanbek.ichat.models.user.UserSignIn;
import com.example.rauansatanbek.ichat.models.user.UserSignUp;
import com.example.rauansatanbek.ichat.network.auth.PostAuthLogin;
import com.example.rauansatanbek.ichat.network.auth.PostAuthRegister;
import com.example.rauansatanbek.ichat.singleton.SingletonSharedPref;
import com.example.rauansatanbek.ichat.views.utils.FileUtils;
import com.flurgle.camerakit.CameraView;
import com.zhihu.matisse.Matisse;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static com.example.rauansatanbek.ichat.singleton.SingletonSharedPref.Key.AUTH_TOKEN;
import static com.example.rauansatanbek.ichat.singleton.SingletonSharedPref.Key.EMAIL;
import static com.example.rauansatanbek.ichat.singleton.SingletonSharedPref.Key.FIRST_NAME;
import static com.example.rauansatanbek.ichat.singleton.SingletonSharedPref.Key.IS_LOGGED_IN;
import static com.example.rauansatanbek.ichat.singleton.SingletonSharedPref.Key.LEST_NAME;
import static com.example.rauansatanbek.ichat.singleton.SingletonSharedPref.Key.USER_AVATAR;
import static com.example.rauansatanbek.ichat.singleton.SingletonSharedPref.Key.USER_ID;

public class LoginActivity extends AppCompatActivity implements PostAuthLogin.PostAuthLoginInterface,
        PostAuthRegister.PostAuthRegisterInterface {

    ActivityLoginBinding activityLoginBinding;
    View mDecorView;

    //fragment transaction
    FragmentTransaction fragmentTransaction;

    //Fragments
    FragmentSignUp fragmentSignUp;
    FragmentSignIn fragmentSignIn;

    //singleton shared pref
    SingletonSharedPref sharedPref;

    //Matisse
    Uri mPhoto;

    //test
    CameraView cameraView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.camera_test);
        activityLoginBinding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        mDecorView = getWindow().getDecorView();

        //singleton shared pref
        if(sharedPref == null)
            sharedPref = SingletonSharedPref.getInstance();

        //begin transaction
        runTransaction(Consts.FRAGMENT_ADD, Consts.FRAGMENT_SIGN_IN);

        //test
//        cameraView = (CameraView) findViewById(R.id.camera);

    }

    //test
    @Override
    protected void onResume() {
        super.onResume();
//        cameraView.start();
//
//        cameraView.setCameraListener(new CameraListener() {
//            @Override
//            public void onPictureTaken(byte[] picture) {
//                super.onPictureTaken(picture);
//                Log.d("MyLogs", "onPictureTaken");
//                // Create a bitmap
//                Bitmap result = BitmapFactory.decodeByteArray(picture, 0, picture.length);
//                ImageView imageView = (ImageView) findViewById(R.id.imageView3);
//                imageView.setImageBitmap(result);
//            }
//        });

    }

    public void takePicture(View v) {
        cameraView.captureImage();
    }
    @Override
    protected void onPause() {
//        cameraView.stop();
        super.onPause();
    }

    /** begin transaction */
    public void runTransaction(int type, int fragmentId) {
        //Fragments
        fragmentSignUp = new FragmentSignUp();
        fragmentSignIn = new FragmentSignIn();

        //Fragment transaction
        fragmentTransaction = getSupportFragmentManager().beginTransaction();

        //switch fragment add, replace
        switch (type) {
            case Consts.FRAGMENT_ADD:
                fragmentTransaction
                        .add(R.id.login_container, fragmentId == Consts.FRAGMENT_SIGN_IN ?
                                fragmentSignIn : fragmentSignUp);
                break;
            case Consts.FRAGMENT_REPLACE:
                fragmentTransaction
                        .replace(R.id.login_container, fragmentId == Consts.FRAGMENT_SIGN_IN ?
                                fragmentSignIn : fragmentSignUp);
                break;
            case Consts.FRAGMENT_REMOVE:
                //remove sign in
                fragmentTransaction
                        .remove(fragmentId == Consts.FRAGMENT_SIGN_IN ?
                                fragmentSignIn : fragmentSignUp);
                break;
        }
        fragmentTransaction.commit();
    }

    /**
     * hiding status bar and
     * bottom navigation bar
     */
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
//        if (hasFocus) {
//            mDecorView.setSystemUiVisibility(
//                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
//                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
//                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
//                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
//                            | View.SYSTEM_UI_FLAG_FULLSCREEN
//                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
//        }
    }

    /** go to chat activity */
    public void goToChatActivity() {
        Intent intent = new Intent(this, ChatActivity.class);
        startActivity(intent);
        finish();
    }

    /** show snack bar when error */
    public void showSnackBar(String text) {
        Snackbar snackbar = Snackbar
                .make(activityLoginBinding.loginContainer, text, Snackbar.LENGTH_LONG);

        // Changing action button text color
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextAppearance(getApplicationContext(), R.style.fontFamily);
        textView.setTextColor(Color.YELLOW);
        snackbar.show();
    }

    /** Register */
    public void authRegister(UserSignUp signUp) {
        if(mPhoto != null) {
            File file = FileUtils.getFile(this, mPhoto);

            // create RequestBody instance from file
            RequestBody requestFile =
                    RequestBody.create(MediaType.parse("multipart/form-data"), file);

            // MultipartBody.Part is used to send also the actual file name
            MultipartBody.Part photo =
                    MultipartBody.Part.createFormData("photo", file.getName(), requestFile);


            PostAuthRegister register = new PostAuthRegister(this, signUp, photo);
            register.postAuthRegister();
        } else {
            signUp.setTypeOfSignUp(Consts.SIGNUP_WITHOUT_PHOTO);
            PostAuthRegister register = new PostAuthRegister(this, signUp, null);
            register.postAuthRegister();
        }
        Log.d("MyLogs", "Register: " + signUp.firstName + "\n" +
                signUp.lastName + "\n" +
                signUp.email + "\n" +
                signUp.password + "\n" +
                signUp.confirmPassword);

        //remove
//        goToChatActivity();
    }

    /** Login */
    public void authLogin(UserSignIn signIn) {
        PostAuthLogin login = new PostAuthLogin(this, signIn);
        login.postAuthLogin();

        Log.d("MyLogs", "Login: " + signIn.email + "\n" +
                signIn.password);

        //remove
//        Intent intent = new Intent(this, MainActivity.class);
//        startActivity(intent);
    }

    /** this method will be called
     * when we got response from server in the Activity or Fragment*/
    @Override
    public void postAuthRegisterInterface(AuthModel authModel) {
        fragmentSignUp.stopProcess();

        if(authModel != null) {
            //Save data in shared pref
            saveInShared(authModel);

            //go to list activity
            goToChatActivity();
        } else {
            showSnackBar("Такой логин уже существует");
        }
    }

    /** this method will be called
     * when we got response from server in the Activity or Fragment*/
    @Override
    public void postAuthLoginInterface(AuthModel authModel) {
        fragmentSignIn.stopProcess();
        if(authModel != null) {
            //Save data in shared pref
            saveInShared(authModel);
            //go to list activity
            goToChatActivity();
        } else {
            showSnackBar(getString(R.string.wrong_pass_or_login));
        }
    }

    /** Save data in shared pref */
    private void saveInShared(AuthModel authModel) {
        sharedPref.put(AUTH_TOKEN, "Token " + authModel.getToken());
        sharedPref.put(EMAIL, authModel.getEmail());
        sharedPref.put(FIRST_NAME, authModel.getFirstName());
        sharedPref.put(LEST_NAME, authModel.getLastName());
        sharedPref.put(USER_ID, authModel.getId());
        sharedPref.put(USER_AVATAR, authModel.getPhoto());
        sharedPref.put(IS_LOGGED_IN, true);
    }



    /** onActivityResult Matisse*/
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Consts.REQUEST_CODE_CHOOSE && resultCode == RESULT_OK) {
            mPhoto = Matisse.obtainResult(data).get(0);
            fragmentSignUp.setAvatar(mPhoto);
        }
    }
}
