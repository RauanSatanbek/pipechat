package com.example.rauansatanbek.ichat.activities.chat;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.speech.RecognizerIntent;
import android.support.design.widget.AppBarLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.example.rauansatanbek.ichat.Consts;
import com.example.rauansatanbek.ichat.R;
import com.example.rauansatanbek.ichat.activities.auth.LoginActivity;
import com.example.rauansatanbek.ichat.databinding.ActivityChatBinding;
import com.example.rauansatanbek.ichat.fragments.FragmentChat;
import com.example.rauansatanbek.ichat.fragments.FragmentContacts;
import com.example.rauansatanbek.ichat.models.chat.DialogList;
import com.example.rauansatanbek.ichat.models.chat.Message;
import com.example.rauansatanbek.ichat.models.chat.MessageFromSocket;
import com.example.rauansatanbek.ichat.models.user.UserList;
import com.example.rauansatanbek.ichat.network.chat.PostCreateDialog;
import com.example.rauansatanbek.ichat.service.MessageService;
import com.example.rauansatanbek.ichat.singleton.SingletonSharedPref;
import com.example.rauansatanbek.ichat.views.adapters.ChatViewPagerAdapter;
import com.example.rauansatanbek.ichat.views.adapters.ContactsAdapter;
import com.example.rauansatanbek.ichat.views.utils.FileUtils;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.util.AbstractDrawerImageLoader;
import com.mikepenz.materialdrawer.util.DrawerImageLoader;
import com.squareup.picasso.Picasso;
import com.zhihu.matisse.Matisse;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import id.zelory.compressor.Compressor;

import static com.example.rauansatanbek.ichat.singleton.SingletonSharedPref.Key.AUTH_TOKEN;
import static com.example.rauansatanbek.ichat.singleton.SingletonSharedPref.Key.DIALOG_ID;
import static com.example.rauansatanbek.ichat.singleton.SingletonSharedPref.Key.EMAIL;
import static com.example.rauansatanbek.ichat.singleton.SingletonSharedPref.Key.FIRST_NAME;
import static com.example.rauansatanbek.ichat.singleton.SingletonSharedPref.Key.IS_LOGGED_IN;
import static com.example.rauansatanbek.ichat.singleton.SingletonSharedPref.Key.LEST_NAME;
import static com.example.rauansatanbek.ichat.singleton.SingletonSharedPref.Key.USER_AVATAR;
import static com.example.rauansatanbek.ichat.singleton.SingletonSharedPref.Key.USER_ID;

public class ChatActivity extends AppCompatActivity implements
        ContactsAdapter.ContactsOnClick,
        PostCreateDialog.PostCreateDialogInterface,
        Drawer.OnDrawerItemClickListener{

    View mDecorView;
    ActivityChatBinding chatBinding;

    FragmentContacts fragmentContacts; //fragment
    FragmentChat fragmentChat;
    SingletonSharedPref sharedPref; //shared pref
    ChatViewPagerAdapter adapter; //ViewPager adapter
    List<Uri> mSelected; //Matisse

    //toolbar
    Toolbar mToolbar;
    AppBarLayout appBarLayout;
    ViewPager viewPager;

    String TOKEN_USER = ""; //user token
    BroadcastReceiver mBroadcastReceiver;
    //remove
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        chatBinding = DataBindingUtil.setContentView(this, R.layout.activity_chat);

        //singleton shared pref
        if(sharedPref == null)
            sharedPref = SingletonSharedPref.getInstance(this);

        TOKEN_USER = sharedPref.getString(AUTH_TOKEN, "Token empty"); // user token
        Log.d("MyLogs", TOKEN_USER);
        //check user is logged inMy
        if (!sharedPref.getBoolean(IS_LOGGED_IN, false)) {
            goToLoginActivity();
        }

        //toolbar
        appBarLayout = chatBinding.appBarLayout;
        mToolbar = chatBinding.toolbarChat;
        setSupportActionBar(mToolbar);

        initMaterialDrawer(); //Material Drawer

        // Set Tabs inside Toolbar
        viewPager = chatBinding.viewPagerChat;
        chatBinding.tabs.setupWithViewPager(viewPager);

        //ViewPager adapter
        adapter = new ChatViewPagerAdapter(getSupportFragmentManager());

        fragmentContacts = new FragmentContacts();
        fragmentChat = new FragmentChat();
        adapter.addFragment(fragmentContacts, "CONTACTS");
        adapter.addFragment(fragmentChat, "CHAT");

        //set adapter to viewPager
        chatBinding.viewPagerChat.setAdapter(adapter);

        // Socket
//        OkHttpClient client = new OkHttpClient();
//        RealTimeClient realTimeClient = new RealTimeClient(TOKEN_USER);
//        RealTimeListener webSocketListener = new RealTimeListener(this);
//
//        client.newWebSocket(realTimeClient.getRequest(), webSocketListener);
//        client.dispatcher().executorService().shutdown();

        // Service
        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                int status = intent.getIntExtra("status", -1);
                if (status != -1) {
                    switch (status) {
                        case Consts.SOCKET_CREATE_MESSAGE:
                            Log.d("MyLogs", "onReceive");
                            try {
                                MessageFromSocket messageFromSocket = (MessageFromSocket)intent.getSerializableExtra("message");
                                Message message = messageFromSocket.getMessage();
                                message.setComeFrom(Consts.MESSAGE_COME_FROM_SOCKET);
                                if (fragmentChat != null) {
                                    if (viewPager.getCurrentItem() == 1 && sharedPref.getInt(DIALOG_ID) == message.getDialog()) {
                                        fragmentChat.addMessage(message);
                                    } else {
                                        incrementMessageCount(message);
                                    }
                                }
                            } catch(Exception e) {
                                Log.d("ChatActivity", "SOCKET_CREATE_MESSAGE: " + e.toString());
                            }

                            break;
                        case Consts.SOCKET_USER_ONLINE: //user online -> add to RecyclerView
                            UserList userOnline = (UserList) intent.getSerializableExtra("user");
                            if (fragmentContacts != null && userOnline != null) {
                                try {
                                    fragmentContacts.adapter.addItem(userOnline);
                                } catch (Exception e) {
                                    Log.d("ChatActivity", "SOCKET_USER_ONLINE: " + e.toString());
                                }
                            }
                            break;
                        case Consts.SOCKET_USER_OFFLINE: //user offline -> remove from RecyclerView
                            UserList userOffline = (UserList) intent.getSerializableExtra("user");
                            if (userOffline != null) {
                                try {
                                    fragmentContacts.adapter.removeItem(userOffline.getId());
                                } catch (Exception e) {
                                    Log.d("ChatActivity", "SOCKET_USER_OFFLINE: " + e.toString());
                                }
                            }
                    }

                }
            }
        };

        IntentFilter filter = new IntentFilter("pipe.chat.message.service"); //service inflater
        registerReceiver(mBroadcastReceiver, filter); //register

        //start service
        Intent intent = new Intent(ChatActivity.this, MessageService.class);
        if (sharedPref.getBoolean(IS_LOGGED_IN, false)) {
            startService(intent);
        }

    }

    /**Increment message count*/
    void incrementMessageCount(Message message) {
        Log.d("MyLogs", "incrementMessageCount");
        try {
            if (message.getSender() != sharedPref.getInt(USER_ID) && fragmentContacts != null) {
                fragmentContacts.adapter.contactNewMessage(message);
            }
        } catch(Exception e) {
            Log.d("ChatActivity", "incrementMessageCount: " + e.toString());
        }
    }

    /**init material drawer*/
    void initMaterialDrawer() {
        String userName = sharedPref.getString(FIRST_NAME) + " " + sharedPref.getString(LEST_NAME);
        String userEmail = sharedPref.getString(EMAIL);
        String userAvatar = sharedPref.getString(USER_AVATAR, "https://gitlab.com/uploads/user/avatar/56386/tt_avatar_small.jpg");

        DrawerImageLoader.init(new AbstractDrawerImageLoader() {
            @Override
            public void set(ImageView imageView, Uri uri, Drawable placeholder) {
                Log.d("MyLogs", "DrawerImageLoader set");
                Picasso.with(imageView.getContext()).load(uri).placeholder(placeholder).into(imageView);
            }

            @Override
            public void cancel(ImageView imageView) {
                Log.d("MyLogs", "DrawerImageLoader cancel");
                Picasso.with(imageView.getContext()).cancelRequest(imageView);
            }
        });

        ProfileDrawerItem profileDrawerItem = new ProfileDrawerItem()
                .withName(userName)
                .withEmail(userEmail)
                .withIcon(userAvatar);

        // Create the AccountHeader
        AccountHeader headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.drawable.nav_drawer_header)
                .addProfiles(profileDrawerItem)
                .build();

        SecondaryDrawerItem logout = new SecondaryDrawerItem ()
                .withIdentifier(Consts.DRAWABLE_ITEM_LOGOUT)
                .withName("Logout")
                .withIcon(R.drawable.ic_logout);
        //Drawer
        Drawer drawer = new DrawerBuilder()
                .withActivity(this)
                .withAccountHeader(headerResult)
                .addDrawerItems(logout)
                .withOnDrawerItemClickListener(this)
                .build();
    }

   /** CustomCompressImage
     * used to compress image
     * 1.5MB -> 30KB + -
     */
    public File customCompressImage(File image) {
        // Compress image in main thread using custom Compressor
        File compressedImage = new Compressor.Builder(this)
                .setMaxWidth(640)
                .setMaxHeight(480)
                .setQuality(75)
                .setCompressFormat(Bitmap.CompressFormat.PNG)
                .setDestinationDirectoryPath(Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES).getAbsolutePath())
                .build()
                .compressToFile(image);

        return compressedImage;
    }


   /** goToLoginActivity
     */
    public void goToLoginActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

   /** OnBackPressed
     * if attach file opened -> close
     * else if CHAT -> CONTACTS
     */
    @Override
    public void onBackPressed() {
        if (fragmentChat.isAttachFile) {
            fragmentChat.hideViews();
        } else if (viewPager.getCurrentItem() == 1) {
            viewPager.setCurrentItem(0);
        } else{
            super.onBackPressed();
        }
    }


   /** Contacts Holder
     * onClick to one contact
     * callBack function
     */
    @Override
    public void contactsOnClick(int id) {
        PostCreateDialog postCreateDialog = new PostCreateDialog(this, TOKEN_USER, id);
        postCreateDialog.postCreateDialog();

        fragmentContacts.setScrollPosition();
        Log.d("MyLogs", "contactsOnClick: " + id);

    }

   /** PostDialogCreated
     * callBack called when we get response from server
     */
    @Override
    public void postCreateDialogInterface(DialogList dialogList) {
        if (dialogList != null) {
            sharedPref.put(DIALOG_ID, dialogList.getDialog().getId());
            Log.d("MyLogs", "Create: DIALOG_ID: " + dialogList.getDialog().getId() + " " +
                    sharedPref.getInt(DIALOG_ID));
            fragmentChat.loadMessages(dialogList.getMessages());
            viewPager.setCurrentItem(1);
        }
    }

    /**MaterialDrawer onItemClick
     * 1) if position == DRAWABLE_ITEM_LOGOUT -> Logout
     */
    @Override
    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
        switch (position) {
            case Consts.DRAWABLE_ITEM_LOGOUT:
                Log.d("MyLogs", "DRAWABLE_ITEM_LOGOUT");
                sharedPref.put(IS_LOGGED_IN, false);
                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                finish();
                break;
        }
        return false;
    }


    /** On activity result
     * Matisse && AttachFile
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && null != data) {
            switch (requestCode) {
                case Consts.REQUEST_CODE_CHOOSE:
                    mSelected = Matisse.obtainResult(data);
                    break;
                case Consts.REQ_CODE_SPEECH_INPUT:
                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    Message message = null;
                    if (!result.get(0).equals("") && result.get(0) != null) {
                        message = new Message(result.get(0));
                        message.setType(Consts.MESSAGE_TEXT);
                        fragmentChat.sendMessage(message);
                    }
                    break;
                case Consts.ATTACHED_FILE:
                    Log.d("MyLogs", "ATTACHED_FILE");
                    mSelected = Matisse.obtainResult(data);
                    Message msg = new Message("Cool!!!");
                    msg.setType(Consts.MESSAGE_PHOTO);
                    msg.setPhotoUri(mSelected);

                    //test
//                  File image = FileUtil.from(this,  mSelected.get(0));
                    File image = FileUtils.getFile(this, mSelected.get(0));
                    msg.setImage(customCompressImage(image));

                    msg.setTypeOfPhoto(Consts.PHOTO_FROM_GALLERY);
                    fragmentChat.sendMessage(msg);
                    fragmentChat.hideViews();
                    break;
            }
        }
    }


    @Override
    protected void onRestart() {
        super.onRestart();

        //singleton shared pref
        if(sharedPref == null){
            sharedPref = SingletonSharedPref.getInstance(this);
            Log.d("MyLogs", "Shared is null");
        }
    }

}


