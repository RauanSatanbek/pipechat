package com.example.rauansatanbek.ichat.views.holders;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.rauansatanbek.ichat.databinding.ChatMessageItemBinding;

/**
 * Created by Rauan Satanbek on 02.05.2017.
 */

public class ChatMsgDialogViewHolder extends RecyclerView.ViewHolder{
    public ChatMessageItemBinding chatBinding;
    public ChatMsgDialogViewHolder(View itemView) {
        super(itemView);

        //bind layout
        chatBinding = DataBindingUtil.bind(itemView);
    }
}
