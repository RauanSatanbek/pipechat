package com.example.rauansatanbek.ichat.network.chat;

import android.util.Log;

import com.example.rauansatanbek.ichat.Consts;
import com.example.rauansatanbek.ichat.models.chat.Message;
import com.example.rauansatanbek.ichat.network.ApiClient;
import com.example.rauansatanbek.ichat.network.ApiService;

import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostCreateMessage {
    //body
    String token;
    Message message;
    MultipartBody.Part photo;
    HashMap<String, RequestBody> messagePhoto;
    HashMap<String, String> messageText;
    //interface
    PostCreateMessageInterface mInterface;

    /** Constructor */
    public PostCreateMessage(PostCreateMessageInterface mInterface, String token, Message message) {
        this.mInterface = mInterface;

        //body
        this.token = token;
        this.message = message;

        if (message.getType() == Consts.MESSAGE_PHOTO) {
            messagePhoto = new HashMap<>();
            messagePhoto.put("message", createRequestBody(message.getMessage()));
            messagePhoto.put("dialog", createRequestBody(message.getDialog().toString()));
            messagePhoto.put("type", createRequestBody(message.getType() + ""));

            // create RequestBody instance from file
            RequestBody requestFile =
                    RequestBody.create(MediaType.parse("multipart/form-data"), message.getImage());

            // MultipartBody.Part is used to send also the actual file name
            photo = MultipartBody.Part.createFormData("photo", message.getImage().getName(), requestFile);
        } else if (message.getType() == Consts.MESSAGE_TEXT) {
            messageText = new HashMap<>();
            messageText.put("message", (message.getMessage()));
            messageText.put("dialog", (message.getDialog().toString()));
            messageText.put("type", (message.getType() + ""));
        }
    }

    public interface PostCreateMessageInterface{
        /** this method will be called
         * when we got response from server in the Activity or Fragment*/
        void postCreateMessageInterface(Message message);
    }

    public void postCreateMessage() {
        ApiService service = ApiClient.initApiService();

        Call<Message> call = null;

        if (message.getType() == Consts.MESSAGE_TEXT) {
            call = service.postCreateMessage(token, messageText);
            Log.d("MyLogs", "Hi");
        } else if (message.getType() == Consts.MESSAGE_PHOTO) {
            call = service.postCreateMessagePhoto(token, messagePhoto, photo);
        }
        call.enqueue(new Callback<Message>() {
            @Override
            public void onResponse(Call<Message> call, Response<Message> response) {
                try {
                    Log.d("MyLogs", "postCreateMessage: response.errorBody: " + response.errorBody().string() );
                } catch (Exception e) {}

                Log.d("MyLogs", "postCreateMessage: response: " + response.isSuccessful() + "\n" +
                        response.message());

                //if response success
                if(response.isSuccessful()) mInterface.postCreateMessageInterface(response.body());
                //Or
                else mInterface.postCreateMessageInterface(null);
            }

            @Override
            public void onFailure(Call<Message> call, Throwable t) {
                //if response error
                Log.d("MyLogs", "Throwable: " + t.toString());
                mInterface.postCreateMessageInterface(null);
            }
        });
    }

    RequestBody createRequestBody(String value) {
        return RequestBody.create(MediaType.parse("text"),value.getBytes());
    }

}
