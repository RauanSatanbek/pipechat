package com.example.rauansatanbek.ichat.network.chat;

import android.util.Log;

import com.example.rauansatanbek.ichat.models.chat.DialogList;
import com.example.rauansatanbek.ichat.models.chat.Participants;
import com.example.rauansatanbek.ichat.network.ApiClient;
import com.example.rauansatanbek.ichat.network.ApiService;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Rauan Satanbek on 28.04.2017.
 */

public class PostCreateDialog {
    //body
    String token;
    Participants participants;
//    ArrayList<Integer> participants;
    //interface
    PostCreateDialogInterface mInterface;

    /** Constructor */
    public PostCreateDialog(PostCreateDialogInterface mInterface, String token, int participant) {
        this.mInterface = mInterface;

        //body
        this.token = token;

//        this.participants = new ArrayList<>();
        ArrayList<Integer> list = new ArrayList<>();
        list.add(participant);

        participants = new Participants();
        participants.setParticipants(list);
    }

    public interface PostCreateDialogInterface{
        /** this method will be called
         * when we got response from server in the Activity or Fragment*/
        void postCreateDialogInterface(DialogList dialogList);
    }

    public void postCreateDialog() {
        ApiService service = ApiClient.initApiService();
        Call<DialogList> call = service.postCreateDialog(token, participants);
        call.enqueue(new Callback<DialogList>() {
            @Override
            public void onResponse(Call<DialogList> call, Response<DialogList> response) {
                try {
                    Log.d("MyLogs", "postCreateDialog: response.errorBody: " + response.errorBody().string() );
                } catch (Exception e) {}
                Log.d("MyLogs", "postCreateDialog: response: " + response.isSuccessful() + "\n" +
                            response.message() + " " + response.code());


                //if response success
                if(response.isSuccessful()) mInterface.postCreateDialogInterface(response.body());
                //Or
                else mInterface.postCreateDialogInterface(null);
            }

            @Override
            public void onFailure(Call<DialogList> call, Throwable t) {
                //if response error
                Log.d("MyLogs", "Throwable: " + t.toString());
                mInterface.postCreateDialogInterface(null);
            }
        });
    }
}
