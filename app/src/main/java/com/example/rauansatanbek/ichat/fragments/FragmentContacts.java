package com.example.rauansatanbek.ichat.fragments;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.rauansatanbek.ichat.R;
import com.example.rauansatanbek.ichat.databinding.FragmentContactsBinding;
import com.example.rauansatanbek.ichat.models.user.UserList;
import com.example.rauansatanbek.ichat.network.user.GetUserList;
import com.example.rauansatanbek.ichat.singleton.SingletonSharedPref;
import com.example.rauansatanbek.ichat.views.adapters.ContactsAdapter;

import java.util.List;

import static com.example.rauansatanbek.ichat.singleton.SingletonSharedPref.Key.AUTH_TOKEN;


public class FragmentContacts extends Fragment implements GetUserList.GetUserListInterface, SwipeRefreshLayout.OnRefreshListener{
    FragmentContactsBinding contactsBinding;
    Context context; //context
    SingletonSharedPref sharedPref; //shared pref
    public ContactsAdapter adapter; //adapter
    RecyclerView recyclerView; //recyclerView

    boolean isLoaded = false;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //binding
        contactsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_contacts, container, false);

        //singleton shared pref
        if(sharedPref == null)
            sharedPref = SingletonSharedPref.getInstance();

        //context
        context = getActivity();

        //recyclerView
        recyclerView = (RecyclerView) inflater.inflate(R.layout.recycler_view, container, false);
        LinearLayoutManager manager = new LinearLayoutManager(context);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(manager);

        contactsBinding.recyclerViewContainer.addView(recyclerView);
        contactsBinding.swipeContainer.setOnRefreshListener(this); //swipe onRefresh
        GetUserList getUserList = new GetUserList(this, sharedPref.getString(AUTH_TOKEN));
        getUserList.getUserList();

        return contactsBinding.getRoot();
    }

    public void setScrollPosition() {
        recyclerView.scrollToPosition(0);
    }
    @Override
    public void getUserList(List<UserList> userList) {
        contactsBinding.progressBar.setVisibility(View.GONE);
        if (userList != null) {
            //adapter
            if (!isLoaded) {
                adapter = new ContactsAdapter(context, userList);
                recyclerView.setAdapter(adapter);
                isLoaded = true;
            } else {
                adapter.updateList(userList);
                contactsBinding.swipeContainer.setRefreshing(false);
            }
        } else {
            contactsBinding.nothingToShow.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onRefresh() {
        GetUserList getUserList = new GetUserList(this, sharedPref.getString(AUTH_TOKEN));
        getUserList.getUserList();
    }
}
