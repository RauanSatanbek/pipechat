package com.example.rauansatanbek.ichat.fragments;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.speech.tts.TextToSpeech;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.RelativeLayout;

import com.example.rauansatanbek.ichat.Consts;
import com.example.rauansatanbek.ichat.R;
import com.example.rauansatanbek.ichat.activities.chat.ChatActivity;
import com.example.rauansatanbek.ichat.databinding.FragmentChatBinding;
import com.example.rauansatanbek.ichat.models.chat.Chat;
import com.example.rauansatanbek.ichat.models.chat.ChatController;
import com.example.rauansatanbek.ichat.models.chat.DialogList;
import com.example.rauansatanbek.ichat.models.chat.Message;
import com.example.rauansatanbek.ichat.network.chat.GetDialog;
import com.example.rauansatanbek.ichat.network.chat.PostCreateMessage;
import com.example.rauansatanbek.ichat.singleton.SingletonSharedPref;
import com.example.rauansatanbek.ichat.translation_engine.TranslatorFactory;
import com.example.rauansatanbek.ichat.translation_engine.translators.SpeechToTextConvertor;
import com.example.rauansatanbek.ichat.translation_engine.utils.ConversionCallaback;
import com.example.rauansatanbek.ichat.views.adapters.ChatMsgDialogAdapter;
import com.example.rauansatanbek.ichat.views.adapters.HidingScrollListener;
import com.flurgle.camerakit.CameraListener;
import com.flurgle.camerakit.CameraView;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import static com.example.rauansatanbek.ichat.singleton.SingletonSharedPref.Key.AUTH_TOKEN;
import static com.example.rauansatanbek.ichat.singleton.SingletonSharedPref.Key.DIALOG_ID;
import static com.example.rauansatanbek.ichat.singleton.SingletonSharedPref.Key.USER_ID;

public class FragmentChat extends Fragment implements View.OnClickListener,
        TextToSpeech.OnInitListener, View.OnTouchListener, ConversionCallaback,
        PostCreateMessage.PostCreateMessageInterface, GetDialog.GetDialogInterface {
    FragmentChatBinding chatBinding;

    //context
    Context context;
    ChatActivity chatActivity;

    ChatMsgDialogAdapter adapter; //adapter

    //views
    RecyclerView recyclerView;
    RelativeLayout containerSendFile;

    CameraView attachCamera; //camera view
    Chat chat; //data
    ChatController controller;  //data

    //speech to text
    private static final int TTS = 0;
    private static final int STT = 1;
    private static int CURRENT_MODE = -1;
    TranslatorFactory translatorFactory;
    SpeechToTextConvertor speechToTextConvertor;
    private TextToSpeech tts;

    SingletonSharedPref sharedPref; //shared pref
    public boolean isAttachFile = false;  //attachFile
    String TOKEN = ""; //token
    int CHAT_DIALOG_ID; //dialog id
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //binding
        chatBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_chat, container, false);

        //singleton shared pref
        if(sharedPref == null)
            sharedPref = SingletonSharedPref.getInstance();

        TOKEN = sharedPref.getString(AUTH_TOKEN);
        CHAT_DIALOG_ID = sharedPref.getInt(DIALOG_ID, 0);
        //context
        context = getActivity();
        chatActivity = (ChatActivity) context;

        //Views
        recyclerView = (RecyclerView) inflater.inflate(R.layout.recycler_view, container, false);
        containerSendFile = chatBinding.containerSendFile;

        loadDataToBind(); // data

        LinearLayoutManager manager = new LinearLayoutManager(context);
        manager.setStackFromEnd(true);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(manager);

        recyclerView.setOnScrollListener(new HidingScrollListener() {
            @Override
            public void onHide() {
                if (isAttachFile) hideViews(); // hide attach files
            }
            @Override
            public void onShow() {
                if (isAttachFile) hideViews(); // hide attach files
            }
        });

        chatBinding.recyclerViewContainer.addView(recyclerView);
        chatBinding.sendMessage.setOnClickListener(this);
        chatBinding.attachFile.setOnClickListener(this);
        chatBinding.closeAttachFile.setOnClickListener(this);
        chatBinding.icCameraAlt.setOnClickListener(this);

        //camera view
        attachCamera = chatBinding.attachCamera;

        //speech to text
        tts = new TextToSpeech(getActivity(), this);

        //speech_to_text
        translatorFactory = TranslatorFactory.getInstance();
        chatBinding.speechToText.setOnTouchListener(this);

        //load the last dialog
        if (CHAT_DIALOG_ID != 0) {
            Log.d("MyLogs", "CHAT_DIALOG_ID: +++" + CHAT_DIALOG_ID);
            GetDialog getDialog = new GetDialog(this, TOKEN, CHAT_DIALOG_ID);
            getDialog.getDialog();
        } else {
            chat.setIsGotMessage(true);
        }

        return chatBinding.getRoot();
    }

    void loadDataToBind() {
        //data
        if (controller == null) controller = new ChatController();
        if (chat == null) chat = new Chat();

        //set data
        chatBinding.setChat(chat);
        chatBinding.setController(controller);
    }

    public void loadMessages(List<Message> messages) {
        chat.setIsGotMessage(true);
        if (messages == null) {
            messages = new ArrayList<>();
        }
        //adapter
        adapter = new ChatMsgDialogAdapter(context, messages);
        recyclerView.scrollToPosition(messages.size() - 1);
        recyclerView.setAdapter(adapter);
    }

    public void hideViews() {
        isAttachFile = false;
        containerSendFile.animate().translationY(containerSendFile.getHeight()).setInterpolator(new AccelerateInterpolator(2));
    }

    public void showViews() {
        isAttachFile = true;
        containerSendFile.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2));
    }

    @Override
    public void onResume() {
        super.onResume();
        attachCamera.start();

        attachCamera.setCameraListener(new CameraListener() {
            @Override
            public void onPictureTaken(byte[] picture) {
                super.onPictureTaken(picture);
                //create a file to write bitmap data
                String file_path = Environment.getExternalStorageDirectory().getAbsolutePath() +
                        "/Chat/picture";
                Log.d("MyLogs", "onPictureTaken:--- " + file_path);
                File dir = new File(file_path);
                if (!dir.exists()) {
                    dir.mkdirs();
                }
                File file = new File(dir, "sketchpad" + System.currentTimeMillis() + ".jpg");
                try {
                    FileOutputStream fos = new FileOutputStream(file);
                    fos.write(picture);
                    fos.close();

                    Message msg = new Message("image");
                    msg.setImage(chatActivity.customCompressImage(file));

                    msg.setType(Consts.MESSAGE_PHOTO);
                    msg.setTypeOfPhoto(Consts.PHOTO_FROM_GALLERY);
                    sendMessage(msg);
                    hideViews();
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("MyLogs", "onPictureTaken: Error");
                }
                // Create a bitmap
//                Bitmap result = BitmapFactory.decodeByteArray(picture, 0, picture.length);
            }
        });
    }

    @Override
    public void onPause() {
        attachCamera.stop();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        // Don't forget to shutdown tts!
        if (tts != null) {
            tts.stop();
            tts.shutdown();
        }
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        loadDataToBind();
    }

    @Override
    public void onClick(View v) {
        Log.d("MyLogs", "OnClick");
        switch (v.getId()) {
            case R.id.send_message:
                String text = chatBinding.writeMessage.getText().toString();
                Message message = new Message(text);
                message.setType(Consts.MESSAGE_TEXT);
                sendMessage(message);
                break;
            case R.id.attach_file:
                showViews();
                break;
            case R.id.close_attach_file:
                hideViews();
                break;
            case R.id.ic_camera_alt:
                attachCamera.captureImage();
                break;

        }
    }

    /** Send message */
    public void sendMessage(Message message) {
        tts.speak(message.getMessage(), TextToSpeech.QUEUE_FLUSH, null);

        CHAT_DIALOG_ID = sharedPref.getInt(DIALOG_ID, 0);
        message.setDialog(CHAT_DIALOG_ID);
        message.setSender(sharedPref.getInt(USER_ID));

        final SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss z");
        f.setTimeZone(TimeZone.getTimeZone("UTC"));

        message.setCreatedAt(f.format(new Date()));
        PostCreateMessage postCreateMessage = new PostCreateMessage(this, TOKEN, message);
        postCreateMessage.postCreateMessage();
        addMessage(message);
    }

    public void addMessage(Message message) {
        adapter.addMessageItem(message);
        recyclerView.scrollToPosition(adapter.getMessagesSize() - 1);
        chatBinding.writeMessage.setText("");
    }

    /** Speech to text*/
    @Override
    public void onInit(int status) {

        if (status == TextToSpeech.SUCCESS) {

            int result = tts.setLanguage(new Locale("ru-Hant-US"));
            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "This Language is not supported");
            }

        } else {
            Log.e("TTS", "Initilization Failed!");
        }

    }

    @Override
    public void onCompletion() {
        Log.d("MyLogs", "STT: [onCompletion] ");
    }

    @Override
    public void onErrorOccured(String errorMessage) {
        Log.d("MyLogs", "STT: [error] " + errorMessage);

    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                Log.d("MyLogs", "ACTION_DOWN");
                //Ask translator factory to start speech tpo text convertion
                //Hello There is optional

                ((RelativeLayout) v).setBackground(getResources().getDrawable(R.drawable.stt_circle_active));
                speechToTextConvertor =  (SpeechToTextConvertor) translatorFactory.getTranslator(TranslatorFactory.TRANSLATOR_TYPE.SPEECH_TO_TEXT, this);
                speechToTextConvertor.initialize("Hello There", getActivity());

                CURRENT_MODE = STT;
                return true;
            case MotionEvent.ACTION_UP:
                Log.d("MyLogs", "ACTION_UP");
                ((RelativeLayout) v).setBackground(getResources().getDrawable(R.drawable.stt_circle_normal));
                speechToTextConvertor.stopListening();
                return true;
        }
        return false;
    }

    /**SpeechToTextConvertor
     * When success
     */
    @Override
    public void onSuccess(String result) {
        switch (CURRENT_MODE) {
            case STT:
                if (result != null) {
                    Log.d("MyLogs", "STT: [onSuccess] " + result);
                    Message message = new Message(result);
                    message.setType(Consts.MESSAGE_TEXT);
                    sendMessage(message);
                }
                else
                    Log.d("MyLogs", "STT: [onSuccess] null");
        }
    }

    @Override
    public void postCreateMessageInterface(Message message) {

    }

    @Override
    public void getDialog(DialogList dialogList) {
        if (dialogList != null) {
            if (dialogList.getMessages() != null) {
                loadMessages(dialogList.getMessages());
                for (Message message : dialogList.getMessages()) {
                }
            } else {
                chat.setIsGotMessage(true);
            }
        }
    }

    /** compress image */
//    public File customCompressImage(File image) {
//        Log.d("MyLogs", "File: " + MainActivity.getReadableFileSize(image.length()));
//        // Compress image in main thread using custom Compressor
//        File compressedImage = new Compressor.Builder(this)
//                .setMaxWidth(640)
//                .setMaxHeight(480)
//                .setQuality(75)
//                .setCompressFormat(Bitmap.CompressFormat.WEBP)
//                .setDestinationDirectoryPath(Environment.getExternalStoragePublicDirectory(
//                        Environment.DIRECTORY_PICTURES).getAbsolutePath())
//                .build()
//                .compressToFile(image);
//
//        Log.d("MyLogs", "Compressed File: " + MainActivity.getReadableFileSize(compressedImage.length()));
//        return compressedImage;
//    }
}
