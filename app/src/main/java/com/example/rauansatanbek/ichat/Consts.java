package com.example.rauansatanbek.ichat;

/**
 * Created by Rauan Satanbek on 26.04.2017.
 */

public final class Consts {

    public static final String MONTHS[] = {
            "JANUARY", "FEBRUARY", "MARCH", "APRIL",
            "MAY", "JUNE", "JULE", "AUGUST",
            "SEPTEMBER","OCTOBER", "NOVEMBER", "DECEMBER"
    }; // list of months
    public static final String IF_MESSAGE_SERVICE = "pipe.chat.message.service"; // fragment add


    public static final int FRAGMENT_ADD = 0; // fragment add
    public static final int FRAGMENT_REPLACE = 1; // fragment replace
    public static final int FRAGMENT_REMOVE = 2; // fragment remove

    public static final int FRAGMENT_SIGN_IN = 0; // sign in
    public static final int FRAGMENT_SIGN_UP = 1; // sign up

    public static final int PHOTO_FROM_SERVER = 0; //photo from server
    public static final int PHOTO_FROM_GALLERY = 1; //photo from gallery

    public static final int MESSAGE_TEXT = 0; //types of message -> text
    public static final int MESSAGE_PHOTO = 1; //types of message -> photo
    public static final int MESSAGE_COME_FROM_SOCKET = 1; //message come from socket

    public static final int SIGNUP_WITHOUT_PHOTO = 1; //sign up without photo

    //onActivityResult
    public static final int REQ_CODE_SPEECH_INPUT = 1; //speech to text
    public static final int ATTACHED_FILE = 2; //attach file
    public static final int REQUEST_CODE_CHOOSE = 3;  //Matisse

    public static final int SOCKET_CREATE_MESSAGE = 1;  //Socket create message
    public static final int SOCKET_USER_ONLINE = 2;  //Socket user online
    public static final int SOCKET_USER_OFFLINE = 3;  //Socket user offline

    public static final int DRAWABLE_ITEM_LOGOUT = 1;  //drawable item logout

}
