package com.example.rauansatanbek.ichat.models.user;

import android.content.Context;
import android.databinding.BaseObservable;
import android.util.Log;
import android.view.View;

import com.dd.processbutton.iml.ActionProcessButton;
import com.example.rauansatanbek.ichat.Consts;
import com.example.rauansatanbek.ichat.R;
import com.example.rauansatanbek.ichat.activities.auth.LoginActivity;
import com.example.rauansatanbek.ichat.views.alert_dialog.ShowDataPicker;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.engine.impl.GlideEngine;

public class UserSignUpController extends BaseObservable {

    /** sign in */
    public void signIn(View view){
        Log.d("MyLogs", "signIn: ");
        //finding activity
        Context context = view.getContext();
        LoginActivity loginActivity = (LoginActivity) context;

        //begin transaction
        loginActivity.runTransaction(Consts.FRAGMENT_REPLACE, Consts.FRAGMENT_SIGN_IN);
    }

    /** sign in */
    public void signUp(View view, UserSignUp userSignUp){
        Context context = view.getContext();
        LoginActivity loginActivity = (LoginActivity) context;

        if(userSignUp.check() && userSignUp.checkPassword()) {
            ((ActionProcessButton) view).setProgress(50);

            //register
            loginActivity.authRegister(userSignUp);
        } else if(!userSignUp.check()) {
            loginActivity.showSnackBar(loginActivity.getString(R.string.fill_in_all));
        } else if (!userSignUp.checkPassword()) {
            loginActivity.showSnackBar(loginActivity.getString(R.string.password_not_equals));
        }
    }

    /** show date picker */
    public void showDatePicker(View view, UserSignUp userSignUp){
        Context context = view.getContext();
        LoginActivity loginActivity = (LoginActivity) context;

        new ShowDataPicker(userSignUp)
                .show(loginActivity.getSupportFragmentManager(), null);
    }
    /** show Matisse */
    public void selectImage(View view){
        Context context = view.getContext();
        LoginActivity loginActivity = (LoginActivity) context;

        //Matisse
        Matisse.from(loginActivity)
                .choose(MimeType.ofImage())
                .theme(R.style.Matisse_Zhihu)
                .countable(false)
                .maxSelectable(1)
                .imageEngine(new GlideEngine())
                .forResult(Consts.REQUEST_CODE_CHOOSE);

    }
}
