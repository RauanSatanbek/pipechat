package com.example.rauansatanbek.ichat.network;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {
    public static final String BASE_URL = "https://powerful-mesa-27755.herokuapp.com/";
    public static final String BASE_URL_FOR_LOAD_PHOTO = "https://powerful-mesa-27755.herokuapp.com";
    private static Retrofit retrofit = null;


    /**
     * Init ApiService
     */
    public static ApiService initApiService() {
        if(retrofit == null) {
//            OkHttpClient client = new OkHttpClient.Builder()
//                    .connectTimeout(10000, TimeUnit.SECONDS)
//                    .readTimeout(10000,TimeUnit.SECONDS).build();
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
//                    .client(client)
                    .build();
        }

        return retrofit.create(ApiService.class);
    }
}
