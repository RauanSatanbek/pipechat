package com.example.rauansatanbek.ichat.models.user;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.example.rauansatanbek.ichat.BR;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserSignUp extends BaseObservable {
    @SerializedName("firstName")
    @Expose
    public String firstName;

    @SerializedName("lastName")
    @Expose
    public String lastName;

    @SerializedName("email")
    @Expose
    public String email;

    @SerializedName("password")
    @Expose
    public String password;

    @SerializedName("date_of_birth")
    @Expose
    public String date_of_birth = "2017-01-10";

    public String confirmPassword;
    public int typeOfSignUp = 0;

    @Bindable
    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
        notifyPropertyChanged(BR.firstName);
    }

    @Bindable
    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
        notifyPropertyChanged(BR.lastName);
    }

    @Bindable
    public String getEmail() {
        return this.email;
    }


    @Bindable
    public String getPassword() {
        return this.password;
    }


    public void setEmail(String email) {
        this.email = email;
        notifyPropertyChanged(BR.email);
    }

    public void setPassword(String password) {
        this.password = password;
        notifyPropertyChanged(BR.password);
    }

    @Bindable
    public String getConfirmPassword() {
        return this.confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
        notifyPropertyChanged(BR.confirmPassword);
    }

    public int getTypeOfSignUp() {
        return this.typeOfSignUp;
    }

    public void setTypeOfSignUp(int typeOfSignUp) {
        this.typeOfSignUp = typeOfSignUp;
    }
    /** checking edit text for empty*/
    public boolean check() {
        return email != null && password != null && firstName != null && lastName != null && confirmPassword != null;
    }

    /** checking password == confirmPassword*/
    public boolean checkPassword() {
        if(password != null && confirmPassword != null) {
            return password.equals(confirmPassword);
        }
        return false;
    }
}
