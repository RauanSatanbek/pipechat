
package com.example.rauansatanbek.ichat.models.chat;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DialogList {
    @SerializedName("message")
    @Expose
    private List<Message> messages;
    @SerializedName("dialog")
    @Expose
    private Dialog dialogs;

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

    public Dialog getDialog() {
        return dialogs;
    }

    public void setDialog(Dialog dialogs) {
        this.dialogs = dialogs;
    }
}
