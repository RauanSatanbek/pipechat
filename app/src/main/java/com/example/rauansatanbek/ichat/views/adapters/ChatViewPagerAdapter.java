package com.example.rauansatanbek.ichat.views.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

/**
 * Created by Rauan Satanbek on 05.05.2017.
 */

public class ChatViewPagerAdapter extends FragmentPagerAdapter{
    ArrayList<Fragment> mFragmentList = new ArrayList<>();
    ArrayList<String> mTitleList = new ArrayList<>();
    public ChatViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTitleList.get(position);
    }

    public void addFragment(Fragment fragment, String title) {
        mFragmentList.add(fragment);
        mTitleList.add(title);
    }

}
