package com.example.rauansatanbek.ichat.models.user;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.example.rauansatanbek.ichat.BR;
import com.example.rauansatanbek.ichat.models.chat.Message;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
//{
//
//        "message": {
//        "id": 40,
//        "message": "yfssfghhhcc",
//        "type": 0,
//        "created_at": "2017-05-16T18:51:09.598439Z",
//        "photo": null,
//        "sender": 1,
//        "dialog": 2
//        },
//        "user": {
//            "id": 1,
//            "online": "online",
//            "last_login": null,
//            "email": "galix@gmail.sd8",
//            "first_name": "Galizhan",
//            "last_name": "Tolybayev",
//            "photo": null
//            }
//        }

public class UserList extends BaseObservable implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("last_login")
    @Expose
    private String lastLogin;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("photo")
    @Expose
    private String photo;
    @SerializedName("online")
    @Expose
    private String online;

//    @SerializedName("message")
//    @Expose
    private Message message;
    private int messageCount = 0;
    public int getMessageCount() {
        return messageCount;
    }

    public void setMessageCount(int messageCount) {
        this.messageCount = messageCount;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }
    @Bindable
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
        notifyPropertyChanged(BR.id);
    }

    @Bindable
    public String getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(String lastLogin) {
        this.lastLogin = lastLogin;
        notifyPropertyChanged(BR.lastLogin);
    }

    @Bindable
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
        notifyPropertyChanged(BR.email);
    }

    @Bindable
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
        notifyPropertyChanged(BR.firstName);
    }

    @Bindable
    public Object getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
        notifyPropertyChanged(BR.lastName);
    }

    @Bindable
    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
        notifyPropertyChanged(BR.photo);
    }

    @Bindable
    public String getOnline() {
        return online;
    }

    public void setOnline(String online) {
        this.online = online;
    }
}
