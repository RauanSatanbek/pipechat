package com.example.rauansatanbek.ichat.network.user;

import android.util.Log;

import com.example.rauansatanbek.ichat.models.user.UserList;
import com.example.rauansatanbek.ichat.network.ApiClient;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetUserList {
    String mToken; //token
    GetUserListInterface mInterface; //callback

    public GetUserList(GetUserListInterface mInterface, String mToken) {
        this.mInterface = mInterface;
        this.mToken = mToken;
    }


    /** get list of user interface */
    public interface GetUserListInterface {
        /** this method will be called
         * when we got response from server in the Activity or Fragment*/
        void getUserList(List<UserList> userList);
    }

    public void getUserList() {
        Call<List<UserList>> call = ApiClient.initApiService()
                .getUserList(mToken);

        call.enqueue(new Callback<List<UserList>>() {
            @Override
            public void onResponse(Call<List<UserList>> call, Response<List<UserList>> response) {
                try {
                    Log.d("MyLogs", "response.errorBody: "  + response.errorBody().string());
                } catch (Exception e) {}
                Log.d("MyLogs", "postAuthRegister: response: " + response.isSuccessful() + "\n" +
                    response.message());

                //if response success
                if(response.isSuccessful()) mInterface.getUserList(response.body());
                //Or
                else mInterface.getUserList(null);
            }

            @Override
            public void onFailure(Call<List<UserList>> call, Throwable t) {
                //if response error
                Log.d("MyLogs", "Throwable: " + t.toString());
                mInterface.getUserList(null);
            }
        });
    }

}
