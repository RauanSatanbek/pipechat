package com.example.rauansatanbek.ichat.network.auth;

import android.util.Base64;
import android.util.Log;

import com.example.rauansatanbek.ichat.models.network.auth.AuthModel;
import com.example.rauansatanbek.ichat.models.user.UserSignIn;
import com.example.rauansatanbek.ichat.network.ApiClient;
import com.example.rauansatanbek.ichat.network.ApiService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Rauan Satanbek on 28.04.2017.
 */

public class PostAuthLogin {
    //body
    String token;
//    UserSignIn signIn;

    //interface
    PostAuthLoginInterface mInterface;

    /** Constructor */
    public PostAuthLogin(PostAuthLoginInterface mInterface, UserSignIn signIn) {
        this.mInterface = mInterface;

        //body
        token = "Basic " + Base64.encodeToString(String.format("%s:%s", signIn.getEmail(), signIn.getPassword()).getBytes(), Base64.NO_WRAP);
        Log.d("MyLogs", "PostAuthLogin: " + token);
//        this.signIn = signIn;
    }

    /** Auth register interface */
    public interface PostAuthLoginInterface{
        /** this method will be called
         * when we got response from server in the Activity or Fragment*/
        void postAuthLoginInterface(AuthModel authModel);
    }

    /** Auth login */
    public void postAuthLogin() {
        ApiService service = ApiClient.initApiService();
        Call<AuthModel> call = service.postAuthLogin(token);
        call.enqueue(new Callback<AuthModel>() {
            @Override
            public void onResponse(Call<AuthModel> call, Response<AuthModel> response) {

                try {
                    Log.d("MyLogs", "postCreateDialog: response.errorBody: " + response.errorBody().string() );
                } catch (Exception e) {}

                Log.d("MyLogs", "postAuthRegister: response: " + response.isSuccessful() + "\n" +
                        response.message() + " " + response.errorBody());

                //if response success
                if(response.isSuccessful()) mInterface.postAuthLoginInterface(response.body());
                //Or
                else mInterface.postAuthLoginInterface(null);
            }

            @Override
            public void onFailure(Call<AuthModel> call, Throwable t) {
                //if response error
                Log.d("MyLogs", "Throwable: " + t.toString());
                mInterface.postAuthLoginInterface(null);
            }
        });
    }
}
