package com.example.rauansatanbek.ichat.network.real_time;

import android.util.Log;

import com.example.rauansatanbek.ichat.models.chat.MessageFromSocket;
import com.example.rauansatanbek.ichat.models.user.UserList;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import okio.ByteString;

public class RealTimeListener extends WebSocketListener {
    private static final int NORMAL_CLOSURE_STATUS = 0;
    RealTimeCallBacks callBack;

    public RealTimeListener(RealTimeCallBacks callBack) {
        this.callBack = callBack;
    }

    @Override
    public void onOpen(WebSocket webSocket, Response response) {
    }

    @Override
    public void onMessage(WebSocket webSocket, String text) {
        Log.d("MyLogs","webSocket: " + text);
        Gson gson = new Gson();
        try {
            JSONObject jsonObject = new JSONObject(text);
            String event = jsonObject.getString("event");
            int status = jsonObject.getInt("status");

            if ("Message.Created".equals(event)) {
                String data = jsonObject.getString("data");
                MessageFromSocket message = gson.fromJson(data, MessageFromSocket.class);
                callBack.chatMessageCreated(message);
            } else if ("User.online".equals(event)) {
                String data = jsonObject.getString("data");
                UserList user = gson.fromJson(data, UserList.class);
                callBack.contactsUserOnline(user);

            } else if ("User.offline".equals(event)) {
                String data = jsonObject.getString("data");
                UserList user = gson.fromJson(data, UserList.class);
                callBack.contactsUserOffline(user);
            }

            //TODO "event": "User.online"

        } catch (JSONException e) {
            Log.d("MyLogs","webSocket: Error " + e.toString());
            e.printStackTrace();
        }

    }

    @Override
    public void onMessage(WebSocket webSocket, ByteString bytes) {
        Log.d("MyLogs","Receiving: " + bytes.hex());
    }

    @Override
    public void onClosing(WebSocket webSocket, int code, String reason) {
        webSocket.close(NORMAL_CLOSURE_STATUS, null);
        Log.d("MyLogs","Closing: " + code + " " + reason);
    }

    @Override
    public void onFailure(WebSocket webSocket, Throwable t, Response response) {
        Log.d("MyLogs","WebSocket: onFailure: " + t.getMessage());
    }
}